package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import java.util.List;

public interface SessionDtoRepository extends JpaRepository<SessionDTO, String> {
    @NotNull
    List<SessionDTO> findAllByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);
}

