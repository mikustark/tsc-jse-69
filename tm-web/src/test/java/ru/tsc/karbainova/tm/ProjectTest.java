package ru.tsc.karbainova.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.tsc.karbainova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.karbainova.tm.client.ProjectFeignClient;
import ru.tsc.karbainova.tm.marker.RestCategory;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;

public class ProjectTest {

    private final Project project = new Project("test");

    private final Project project2 = new Project("test2");

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/projects/";


    @BeforeClass
    public static void beforeClass() {

        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add("JSESSIONID" + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Before
    public void before() {
        create(project);
    }

    @Test
    @Category(RestCategory.class)
    public void findTest() {
        Assert.assertEquals(project.getName(), find(project.getId()).getName());
    }

    @Nullable
    private Project find(String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(getHeader());
        @NotNull String url = PROJECT_URL + "find/" + projectId;
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(RestCategory.class)
    public void createTest() {
        create(project);
        @Nullable final Project projectNew = find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @NotNull
    private void create(Project project) {
        @NotNull String url = PROJECT_URL + "create";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(project, getHeader());
        restTemplate.postForEntity(url, httpEntity, Project.class);
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Project updatedProject = find(project.getId());
        updatedProject.setName("updated");
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(updatedProject, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Project.class
        );
        Assert.assertEquals("updated", find(project.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void deleteTest() {
        delete(project.getId());
        Assert.assertNull(find(project.getId()));
    }

    private void delete(String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                PROJECT_URL + "delete/" + projectId;
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Project.class
        );
    }

    @Test
    @Category(RestCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(project2);
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    private List<Project> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = PROJECT_URL + "findAll";
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Project>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Project>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

}
