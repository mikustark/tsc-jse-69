package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.service.IEntityService;
import ru.tsc.karbainova.tm.model.AbstractEntity;

public abstract class AbstractEntityService<E extends AbstractEntity> implements IEntityService<E> {
}
