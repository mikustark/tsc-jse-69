package ru.tsc.karbainova.tm.exception;

import org.jetbrains.annotations.NotNull;

public class UserNotFoundException extends AbstractException {
    @NotNull
    public UserNotFoundException() {
        super("Error. User not found.");
    }
}
