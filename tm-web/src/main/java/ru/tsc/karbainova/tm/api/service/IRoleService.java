package ru.tsc.karbainova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.model.Role;

import java.util.List;

public interface IRoleService extends IEntityService<Role>{
    @NotNull
    @SneakyThrows
    List<Role> findAllByUserId(@NotNull String userId);

    @SneakyThrows
    void clear(@NotNull String userId);

}
