<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Task Manager</title>
    <style>
        td {padding: 5px;border: navy dashed 1px;}
        th {font-weight: 700;text-align: left; background: cornflowerblue;border: black solid 1px;}
        hr {clear: both;}
        .right {float:right;}
    </style>
</head>
<body>
<list>
    <li><a href="/tasks">Tasks</a></li>
    <li><a href="/projects">Projects</a></li>
</list>
<div class="right">
    <sec:authorize access="isAuthenticated()">
        <a href="/logout">Logout</a>
    </sec:authorize>
    <sec:authorize access="!isAuthenticated()">
        <a href="/login">Login</a>
    </sec:authorize>
</div>
<hr>
