<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../include/_header.jsp"/>


<form:form action="/task/edit/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>

    <table>
        <tr>
            <td>Name:</td>
            <td>Status:</td>
            <td>Project:</td>
            <td>Finish date:</td>
            <td>Description:</td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" value="${task.name}"/>
                <form:input type="text" path="name"/>
            </td>
            <td>
                <select name="status">
                    <c:forEach var="st" items="${enumStatus}">
                        <c:choose>
                            <c:when test="${task.status.name() == st.name()}">
                                <option selected>${st.name()}</option>
                            </c:when>
                            <c:otherwise>
                                <option>${st.name()}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <form:select path="status">
                    <form:option value="${null}" label="---"></form:option>
                    <form:options items="${statuses}" itemLabel="displayName"></form:options>
                </form:select>
            </td>
            <td>
                <input type="text" name="description" value="${task.description}"/>
                <form:select path="projectId">
                    <form:option value="${null}" label="---"></form:option>
                    <form:options items="${projects}" itemLabel="name" itemValue="id"></form:options>
                </form:select>
            </td>
            <td>
                <form:input type="date" path="endDate"/>
            </td>
            <td>
                <form:input type="text" path="description"/>
            </td>
        </tr>
    </table>
    <button type="submit">Submit</button>
    </form>
    <jsp:include page="../include/_footer.jsp" />
</form:form>
<jsp:include page="../include/_footer.jsp"/>

