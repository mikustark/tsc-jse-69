package ru.tsc.karbainova.tm.api.entity;

import ru.tsc.karbainova.tm.endpoint.Status;


public interface IHasStatus {
    Status getStatus();

    void setStatus(Status status);
}
