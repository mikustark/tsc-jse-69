package ru.tsc.karbainova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.endpoint.TaskEndpoint;


public abstract class AbstractListenerTask extends AbstractSystemListener {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

}
