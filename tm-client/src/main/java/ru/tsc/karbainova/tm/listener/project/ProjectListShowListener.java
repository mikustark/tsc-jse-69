package ru.tsc.karbainova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListenerProject;

@Component
public class ProjectListShowListener extends AbstractListenerProject {
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Project list");
        projectEndpoint.findAllProject(sessionService.getSession()).stream().forEach(o -> System.out.println(o.getId() + " " + o.getName()));
        System.out.println("[OK]");
    }
}
